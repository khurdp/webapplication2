##This is a project build during the Asp.net core (Asp.net 5), MVC 6, C#, Angular 4 and EF course.

The project was build in 2016 so I have upgraded from .net core 1.0 to 2.0.

Following are some methods that were upgraded too since they were deprecated:

1. app.UseAuthentication is now needed in Configure.                    <-- need to confirm since below method call seems same.
2. app.UseIdentity => AuthAppBuilderExtension.UseAuthentication(app)
3. UseFacebookAuthentication in Configure => AddFacebook in ConfigureServices
4. added scoped  ProfileRepository instead of singleton - can't have a singleton ProfileRepository for a scoped ProfileContext.
5. updated sendgrid and twilio call methods

---
##Project features:

1. asp.net core 5.0
2. MVC 6.0
3. EF core 2.0.3
4. SQL server 12.0
5. Web API
6. Angular
7. Repository pattern
8. MVVM pattern
9. Singleton pattern
10. Dependency Injection
11. user management framework - Facebook, Google, linked-in, Twitter authentication as well as local database authentication. Password reset, password saved as password hash, two-way authentication etc.. User .net identity.
12. SendGrid for email
13. Twilio for SMS
14. semanticUI
15. Bower
16. Gulp
17. Azure
18. Kudu
19. PAAS

Development platform: Visual Studio 2017, .net core 2.0, CentOS 7 linux host / IE11 Win7 virtualbox guest (downloaded from Microsoft).

---

##To do:
1. Write unit tests
2. Add Google and LinkedIn authentication
2. Host on Heroku instead of Azure and use Sql server hosted on gearhost - totally free hosting :-)
3. Use bootstrap instead of semanticUI



--
https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

