﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication2.Models
{
    public static class Initializer
    {
        public static void InitializeContext(ProfileContext context)
        {
            context.Database.EnsureCreated();
           
            if (context.Individuals.Any())
            {
                return;   
            }

            // Individuals
            var individual = new Individual
            {
                FullName = "Carson",
                DateOfBirth = DateTime.Now,
                Address = "4437 Texas Street",
                AspNetUserId = "508ba9cc-bd46-40fe-8d25-d1ebb0ee5e1f",
                State = "TX",
                City = "Allen",
                ZipCode = "75254"
            };

            context.Individuals.Add(individual);
            context.SaveChanges();

            // Organizations
            var organization = new Organization
            {
                BusinessName = "IT Group",
                HireDate = DateTime.Now,
                Address = "205 Joiner Street",
                AspNetUserId = "508ba9cc-bd46-40fe-8d25-d1ebb0ee5e1f",
                State = "TX",
                Profession = "Developer",
                City = "Allen",
                ZipCode = "75254"
            };
            context.Organizations.Add(organization);
            context.SaveChanges();
            
            // Hobbies
            var hobby = new Hobbies
            {
                HobbieId = Guid.NewGuid(),
                Name = "Exercise",
                Rating = 5,
                AspNetUserId = "508ba9cc-bd46-40fe-8d25-d1ebb0ee5e1f",
            };
            context.Hobby.Add(hobby);
            context.SaveChanges();

         
           
        }
    }
}