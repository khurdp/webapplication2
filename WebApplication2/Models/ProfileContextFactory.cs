﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.EntityFrameworkCore.Infrastructure;
using WebApplication2.Models;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace WebApplication2.Models
{
    public class ProfileContextFactory : IDesignTimeDbContextFactory<ProfileContext>
    {
        public ProfileContextFactory()
        {
        }

        public ProfileContextFactory(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; set; }

        public ProfileContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ProfileContext>();
            if (Configuration == null)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json")
                    .Build();
                var builder = new DbContextOptionsBuilder<ProfileContext>();
                this.Configuration = configuration;
            }

            optionsBuilder.UseSqlServer(Configuration["ConnectionStrings:DefaultConnection"]);

            return new ProfileContext(optionsBuilder.Options);
        }
    }
}
