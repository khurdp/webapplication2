﻿using Microsoft.AspNetCore.Mvc;

namespace WebApplication2.ViewComponents
{
    public class LoginViewComponent:ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            return View();
        }
    }
}
