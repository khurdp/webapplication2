﻿using System.Threading.Tasks;

namespace WebApplication2.Services
{
   public interface ISmsSend
   {
       Task SendSmsAsync(string number, string message);
   }
}
