﻿using System.Threading.Tasks;

namespace WebApplication2.Services
{
    public interface IEmailSend
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
