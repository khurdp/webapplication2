﻿namespace WebApplication2.Services
{
    public class MessageSenderOptions
    {
        public string SendGridApiKey { get; set; }
        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
    }
}
