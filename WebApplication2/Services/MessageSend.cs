﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;
using WebApplication2.Models;
//using Twilio.Clients;

namespace WebApplication2.Services
{
    public class MessageSend : IEmailSend, ISmsSend
    {
        public MessageSend(IOptions<MessageSenderOptions> optionsAccessor)
        {
            Options = optionsAccessor.Value;
        }

        public MessageSenderOptions Options { get; set; }

        public Task SendEmailAsync(string email, string subject, string message)
        {
//            https://github.com/sendgrid/sendgrid-csharp
            var apiKey = Options.SendGridApiKey;
            var client = new SendGridClient(apiKey);
            var from = new EmailAddress("joe@example.com", "Application");
            var to = new EmailAddress(email, email);
            var msg = MailHelper.CreateSingleEmail(from, to, subject, message, message);

            return client.SendEmailAsync(msg);
        }

        public async Task SendSmsAsync(string number, string message)
        {
            using (var client = new HttpClient { BaseAddress = new Uri("https://api.twilio.com") })
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic",
                    Convert.ToBase64String(Encoding.ASCII.GetBytes($"{Options.AccountSid}:{Options.AuthToken}")));

                var contentSms = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("To",$"+{number}"),
                    new KeyValuePair<string, string>("From", "+15005550006"),
                    new KeyValuePair<string, string>("Body", message)
                });

                var results = await client.PostAsync($"/2010-04-01/Accounts/{Options.AccountSid}/Messages.json", contentSms).ConfigureAwait(false);
            }
        }
    } 

}
