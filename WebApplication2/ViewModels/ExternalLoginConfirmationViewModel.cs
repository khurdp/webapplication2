﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication2.ViewModels
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
