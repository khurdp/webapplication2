﻿using System.Collections.Generic;
using WebApplication2.Models;

namespace WebApplication2.ViewModels
{
    public class DashboardViewModel
    {
        public List<Individual> Individuals { get; set; }

        public List<Organization> Organization { get; set; }

        public List<Hobbies> Hobby { get; set; }
    }
}
