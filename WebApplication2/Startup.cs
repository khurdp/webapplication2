﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.Facebook;
using WebApplication2.Services;
using WebApplication2.Models;
using WebApplication2.Repository;

namespace WebApplication2
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
//            services.AddTransient<IMyInjectedService, MyInjectedService>();
//            services.AddSingleton<IConfiguration>(Configuration);
            services.AddDbContext<ProfileContext>(options => options.UseSqlServer(Configuration["ConnectionStrings:DefaultConnection"]));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ProfileContext>()
                .AddDefaultTokenProviders();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(o => o.LoginPath = new PathString("/login"))
                .AddFacebook(o =>
                    {
                        o.AppId = Configuration["Authentication:Facebook:Id"];
                        o.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
                    });

            services.AddMvc();
            services.AddTransient<IEmailSend, MessageSend>();
            services.AddTransient<ISmsSend, MessageSend>();
            services.AddScoped<IProfileRepository, ProfileRepository>();
            services.Configure<MessageSenderOptions>(Configuration.GetSection("MessageSenderDetails"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ProfileContext context)
        {
            app.UseAuthentication();
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Shared/Error");
            }

            app.UseStaticFiles();
            // used alternative below 
            // app.UseIdentity();  // deprecated. Uncomment to see alternative
            AuthAppBuilderExtensions.UseAuthentication(app);  // alternative for app.UseIdentity()

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Main}/{action=Index}/{id?}");
            });


            Initializer.InitializeContext(context);
        }
    }
}
